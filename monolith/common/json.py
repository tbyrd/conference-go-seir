from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):  # if o is an instance of datetime
            return o.isoformat()  #    return o.isoformat()
        else:  # otherwise
            return super().default(o)  #    return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        # if the object to decode is the same class as what's in the #model property, then
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            # create an empty dictionary that will hold the property names as keys and the property values as values
            for (
                property
            ) in self.properties:  # For each name in the property list
                value = getattr(
                    o, property
                )  # Get the value of the property instance
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[
                    property
                ] = value  # Put the key in the dictionary with the property name as the key
            d.update(self.get_extra_data(o))
            return d  # Return the dictionary
        else:  # Otherwise
            return super().default(o)  # This is from the documentation

    def get_extra_data(self, o):
        return {}
