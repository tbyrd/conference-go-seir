from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
from events.models import Conference
from django.views.decorators.http import require_http_methods
import json
import pika


class StatusDetailEncoder(ModelEncoder):
    model = Status
    properties = [
        "name",
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "status",
    ]
    encoders = {"status": StatusDetailEncoder()}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceDetailEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "status": o.status.name,
            "conference": o.conference.name,
        }


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":  # If the request is a GET
        # presentations = Presentation.objects.all()
        presentations = Presentation.objects.filter(  # Get the presentations
            conference_id=conference_id
        )
        return JsonResponse(
            presentations,
            encoder=PresentationListEncoder,
            safe=False,
        )
    else:  # POST METHOD
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                #  status = 400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:  # if method == PUT
        content = json.loads(
            request.body
        )  # Gets the data that was entered into insomnia
        try:
            # if content.get("status"):
            if "status" in content:
                status = Status.objects.get(name=content["status"])
                content["status"] = status
        except Status.DoesNotExist:
            return {
                "message": "Invalid Status, use APPROVED, REJECTED, SUBMITTED"
            }
        Presentation.objects.filter(id=id).update(
            **content
        )  # Filters the object by conference and updates it in the content
        presentation = Presentation.objects.get(id=id)  # Gets the error again
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )


@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    # Set the hostname that we'll connect to
    parameters = pika.ConnectionParameters(host="rabbitmq")
    # Create a connection to RabbitMQ
    connection = pika.BlockingConnection(parameters)
    # Open a channel to RabbitMQ
    channel = connection.channel()
    # Create a queue if it does not exist
    channel.queue_declare(queue="presentation_approvals")
    # Send the message to the queue
    channel.basic_publish(
        exchange="",
        routing_key="presentation_approvals",
        # body="Data from producer",
        body=json.dumps(
            (
                {
                    "presenter_name": presentation.presenter_name,
                    "presenter_email": presentation.presenter_email,
                    "title": presentation.title,
                }
            ),
        ),
    )
    # Print a status message
    # Close the connection to RabbitMQ
    connection.close()

    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_rejections")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_rejections",
        body=json.dumps(
            {
                "presenter_name": presentation.presenter_name,
                "presenter_email": presentation.presenter_email,
                "title": presentation.title,
            }
        ),
    )
    connection.close()
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
