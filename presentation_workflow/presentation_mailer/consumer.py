import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

# Create a function that will process the message when it arrives
def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    presentation = json.loads(body)
    presenter_name = presentation["presenter_name"]
    presentation_title = presentation["title"]
    # handle the messages
    send_mail(
        "Your presentation has been accepted",
        f" { presenter_name } we are happy to tell you that your presentation { presentation_title } has been accepted",
        "admin@conference.go",
        [presentation["presenter_email"]],
        fail_silently=False,
    )


# [body["presenter_email"]],
# f'{body["presenter_name"]}, "we are happy to tell you that your presentation " + body["title"] + " has been accepted"),'


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    presentation_reject = json.loads(body)
    presenter_name = presentation_reject["presenter_name"]
    presentation_title = presentation_reject["title"]
    send_mail(
        "Your presentation has been rejected",
        f" { presenter_name } we are sad to tell you that your presentation { presentation_title } has been rejected",
        "admin@conference.go",
        [presentation_reject["presenter_email"]],
        fail_silently=False,
    )


while True:
    try:
        # Create a main method to run
        # Set the hostname that we'll connect to
        parameters = pika.ConnectionParameters(host="rabbitmq")
        # Create a connection to RabbitMQ
        connection = pika.BlockingConnection(parameters)
        # Open a channel to RabbitMQ
        channel = connection.channel()

        # Create a queue if it does not exist
        channel.queue_declare(queue="presentation_approvals")
        # Configure the consumer to call the process_message function

        # when a message arrives
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue="presentation_rejections")
        # Configure the consumer to call the process_message function
        # when a message arrives
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        # Tell RabbitMQ that you're ready to receive messages
        channel.start_consuming()
    except AMQPConnectionError:
        print("Couldn't connect")
        time.sleep(2.0)
