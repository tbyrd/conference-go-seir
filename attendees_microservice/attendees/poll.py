import json
import requests

from .models import ConferenceVO


def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(
        url
    )  # Make a request and set it equal to the response variable
    content = json.loads(
        response.content
    )  # Changes the JSON into a dictionary
    for conference in content[
        "conferences"
    ]:  # Loop through the conference value in the key value pair
        ConferenceVO.objects.update_or_create(  # update_or_create (checks if there is a chance, if there is it will update else it will create it)
            import_href=conference[
                "href"
            ],  # It will know based on the import_href
            defaults={
                "name": conference["name"]
            },  # This tells it what to update if it finds it
        )
